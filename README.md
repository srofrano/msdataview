_MSDataView_

Steven Rofrano, 2017

This program allows a user to manipulate data contained in an Agilent MassHunter \*.d file.


_Usage Instructions_

1. Go to MSDataView\MSDataView\bin\Release

2. Double click on `MSDataView.exe` to run.


_How to Process a Data File_

1. Click `Select Data` to load a \*.d file to process

2. Click `Select Output` to choose a directory where the output CSV will be saved

3. Add some mass ranges. You can either use the text boxes to add the ranges, or load them from a config file (see below). To add them with the text box, specify either a start and end mass, or the tolerance and the centerpoint mass, which will use the deltaM formula to compute the start and end points. You can remove ranges using the buttons.

4. Click run. The file will be loaded, the chromatograms for each range will be extracted, and the data will be saved in a CSV in the specified directory.


_Saving Range Files_

1. When you have configured the mass ranges to your liking, you may save the ranges at a location of your choosing by clicking the `Save` button.

2. Regardless of the entry method, the ranges will be saved in the (min, max) format.


_Loading Range Files_

1. You can load a list of ranges from any text file. This file need not have been saved by the MSDataView program.

2. To load a (min, max) range file, make sure it follows this format:


```
Min,Max
[min1],[max1]
[min2],[max2]
[min3],[max3]
...

```

Where [minX],[maxX] is an ordered set of numbers (e.g. 1.7,1.9) where the min is smaller than the max.


3. To load a centerpoint range file, make sure it follows this format:

```
Tolerance
[tol]
Masses
[mass1]
[mass2]
[mass3]
...

```

Where [tol] is a number specifying the ppm tolerance that will be applied to all the masses [mass1], [mass2], ... in the list.



