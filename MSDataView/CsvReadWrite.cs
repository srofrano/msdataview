﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using Agilent.MassSpectrometry.DataAnalysis;

namespace MSDataView
{
    //class Class1
    //{
    //}
    public static class CsvReadWrite 
    {
        //public CsvReadWrite()
        //{
        //}
        public static void WriteMinMaxRangesToFile(string fileName, List<IRange> massRangeList)
        {

            string delim = ",";
            StringBuilder csvString = new StringBuilder();
            // Add first row header
            StringBuilder headerRow = new StringBuilder("Min");
            headerRow.Append(delim);
            headerRow.Append("Max");
            csvString.AppendLine(headerRow.ToString());


            var data = new StringBuilder();
            foreach (IRange range in massRangeList)
            {
                var dataLine = new StringBuilder(String.Format("{0}", range.Start));
                dataLine.Append(delim);
                dataLine.AppendFormat("{0}", range.End);
                data.AppendLine(dataLine.ToString());
            }

            // Write the full string to the file
            csvString.AppendLine(data.ToString());
            File.WriteAllText(fileName, csvString.ToString());
        }

        // File must look like this:
        // Tolerance
        // t
        // Masses
        // m1
        // m2
        // ...
        private static List<IRange> ReadCenterpointRangesFromStreamReader(StreamReader reader)
        {
            Console.WriteLine("Reading centerpoint ranges");
            // Already read the header, so start with the tolerance
            var line = reader.ReadLine();
            double tolerance;
            if (!double.TryParse(line, out tolerance))
            {
                throw new Exception("Detected an incorrectly formatted Centerpoint Range file.");
            }
            Console.WriteLine(String.Format("tolerance: {0}", tolerance));
            // Eat the masses header
            line = reader.ReadLine();
            Console.WriteLine("header line: " + line);
            
            var ranges = new List<IRange>();
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                Console.WriteLine("Read a line: " + line);
                double mass;
                if (!double.TryParse(line, out mass))
                {
                    throw new Exception("Detected an incorrectly formatted mass.");
                }
                var range = new CenterWidthPpmRange(mass, 2*tolerance);
                
                ranges.Add(range);
            }
            Console.WriteLine(String.Format("Got {0} ranges in the range lsit", ranges.Count));
            return ranges;
        }

        private static List<IRange> ReadMaxMinRangesFromStreamReader(StreamReader reader)
        {
            Console.WriteLine("Reading min max ranges");
            
            // Already read the header, so continue with data
            var ranges = new List<IRange>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                //Console.WriteLine("Read a line: " + line);
                double start, end;
                if (!double.TryParse(values[0], out start) || !double.TryParse(values[1], out end))
                {
                    throw new Exception("Detected an incorrectly formatted line.");
                }
                // For whatever reason 2* is needed to get the range as calculated in MSDataReader
                //var range = new CenterWidthPpmRange(mass, 2*tolerance);
                var range = new MinMaxRange(start, end);
                
                ranges.Add(range);
            }
            Console.WriteLine(String.Format("Got {0} ranges in the range lsit", ranges.Count));
            return ranges;

        }

        public static List<IRange> ReadRangesFromFile(string fileName)
        {
            string fileString = File.ReadAllText(fileName);
            

            using (var reader = new StreamReader(fileName))
            {
                var line = reader.ReadLine();
                // Check how many commas are in header row to determine range format
                if (line.Count(c => c == ',') == 0)
                {
                    return ReadCenterpointRangesFromStreamReader(reader);
                } else
                {
                    return ReadMaxMinRangesFromStreamReader(reader);
                }
            }
        }
    }
}
