﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Agilent.MassSpectrometry.DataAnalysis;



namespace MSDataView
{
    public partial class Form1 : Form
    {
        private static string dataFolderPath { get; set; }
        public Form1()
        {
            InitializeComponent();
        }

        //Components
        private void openDataFolderDialog_HelpRequest(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void runButton_Click(object sender, EventArgs e)
        {

            progressLabel.Text = "Running...";
            // Do not proceed if form is not filled out correctly
            if (!validateFormOnRun())
            {
                progressLabel.Text = "Invalid form.";
                return;
            }
            try
            {
                List<IRange> massRanges = GetMassesFromListView();
                List<String> dataFolders = GetDataFoldersFromListView();
                string outputFolder = outputFolderLabel.Text;

                int currentFile = 1;
                int nFiles = dataFolders.Count;
                foreach ( String dataFolder in dataFolders )
                {
                    MSDataReader dr = new MSDataReader(dataFolder, outputFolder, massRanges);
                    progressLabel.Text = String.Format("Processing {0} of {1} files...", currentFile++, nFiles);
                }
                progressLabel.Text = "Done.";

            }
            catch (Exception ex)
            {
                progressLabel.Text = "Fix the error to continue.";
                MessageBox.Show(ex.Message, "Warning");
            }

        }

        private void addIntervalButton_Click(object sender, EventArgs e)
        {
            double start = 0, end = 0, tol = 0;
            bool maxMinRange = double.TryParse(startMassTextBox.Text, out start) 
                && double.TryParse(endMassTextBox.Text, out end);
            bool centerPointRange = double.TryParse(startMassTextBox.Text, out start) 
                && double.TryParse(toleranceTextBox.Text, out tol);
            if (centerPointRange)
            {
                double deltaM = start * tol * 1e-6;
                double a = start - deltaM;
                double b = start + deltaM;
                ListViewItem item = new ListViewItem(new[] { a.ToString(), b.ToString() });
                massIntervalListView.Items.Add(item);
                startMassTextBox.Text = null;
                //var interval = new  MinMaxRange(a, b);
                //massIntervalList.Add(interval);

            }
            else if (maxMinRange)
            {
                if (end <= start)
                {
                    MessageBox.Show("The end point must be greater!");
                }
                else
                {
                    ListViewItem item = new ListViewItem(new[] { startMassTextBox.Text, endMassTextBox.Text });
                    massIntervalListView.Items.Add(item);
                    startMassTextBox.Text = null;
                    endMassTextBox.Text = null;
                    //var interval = new  MinMaxRange(start, end);
                    //massIntervalList.Add(interval);
                }
            }
            else
            {
                MessageBox.Show("You have to enter numbers!");
            }
        }

        private void removeIntervalButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in massIntervalListView.SelectedItems)
            {
                massIntervalListView.Items.Remove(item);
            }
        }

        private void saveIntervalsButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<IRange> massRanges = GetMassesFromListView();
                if (massRanges.Count == 0)
                {
                    throw new Exception("You need to enter some ranges before you can save.");
                }
                if (saveIntervalsFileDialog.ShowDialog() == DialogResult.OK)
                {
                    saveIntervalsFileLabel.Text = saveIntervalsFileDialog.FileName;
                    CsvReadWrite.WriteMinMaxRangesToFile(saveIntervalsFileLabel.Text, massRanges);
                }

            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning");
            }

        }


        private void toleranceTextBox_TextChanged(object sender, EventArgs e)
        {
            if (toleranceTextBox.Text.Length != 0)
            {
                endMassTextBox.Enabled = false;
                startMassLabel.Text = "Mass Center:";
                endMassTextBox.Text = "";
            }
            else
            {
                endMassTextBox.Enabled = true;
                startMassLabel.Text = "Start Mass:";
            }

        }

        private void loadIntervalsButton_Click(object sender, EventArgs e)
        {
            if (loadRangeFileDialog.ShowDialog() == DialogResult.OK)
            {
                saveIntervalsFileLabel.Text = loadRangeFileDialog.FileName;
                try
                {
                    List<IRange> rangeList = CsvReadWrite.ReadRangesFromFile(loadRangeFileDialog.FileName);
                    Console.WriteLine(String.Format("Adding masses to list view. mass length: {0}", rangeList.Count));

                    //massIntervalListView.Clear();
                    massIntervalListView.Items.Clear();
                    foreach (IRange range in rangeList)
                    {
                        Console.WriteLine(range.Start.ToString(), range.End.ToString());
                        ListViewItem item = new ListViewItem(new[] { range.Start.ToString(), range.End.ToString() });
                        massIntervalListView.Items.Add(item);
                    }
                    Console.WriteLine(String.Format("Length of listview after adding: {0}", massIntervalListView.Items.Count));
                    //startMassTextBox.Text = null;
                    //endMassTextBox.Text = null;
                    //var interval = new  MinMaxRange(start, end);
                    //massIntervalList.Add(interval);

                } catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Warning");
                }
            }

        }

            //if (loadRangeFileDialogOld.ShowDialog() == DialogResult.OK)
            //{
            //    saveIntervalsFileLabel.Text = saveIntervalsFileDialog.FileName;
            //    try
            //    {
            //        List<IRange> rangeList = CsvReadWrite.ReadRangesFromFile(loadRangeFileDialogOld.FileName);

            //        massIntervalListView.Clear();
            //        foreach (IRange range in rangeList)
            //        {
            //            ListViewItem item = new ListViewItem(new[] { range.Start.ToString(), range.End.ToString() });
            //            massIntervalListView.Items.Add(item);
            //        }
            //        //startMassTextBox.Text = null;
            //        //endMassTextBox.Text = null;
            //        //var interval = new  MinMaxRange(start, end);
            //        //massIntervalList.Add(interval);

            //    } catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message, "Warning");
            //    }
            //}
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }


        // Helper Methods
        private bool validateFormOnRun()
        {
            List<string> warnings = new List<string>();
            if (outputFolderLabel.Text == "")
            {
                warnings.Add("- You need to select an output folder!\n");
            }
            if (dataFolderListView.Items.Count == 0)
            {
                warnings.Add("- You need to add at least one data folder\n");
            }
            if (massIntervalListView.Items.Count == 0)
            {
                warnings.Add("- You need to add at least one mass range!\n");
            }
            // Concatenate and display the warning messages
            if (warnings.Count > 0)
            {
                StringBuilder errorText = new StringBuilder();
                foreach (string msg in warnings)
                {
                    errorText.Append(msg);
                }
                MessageBox.Show(errorText.ToString(), "Form Error");
                return false;
            }
            return true;
        }
        
        private List<IRange> GetMassesFromListView()
        {
            var massRanges = new List<IRange>();
            foreach(ListViewItem item in massIntervalListView.Items)
            {
                // Get the ranges out of the list view and add them to a list of IRanges
                double start, end;
                double.TryParse(item.SubItems[0].Text, out start);
                double.TryParse(item.SubItems[1].Text, out end);
                var interval = new MinMaxRange(start, end);
                massRanges.Add(interval);
            }
            return massRanges;
        }

        private void loadRangeFileDialog_FileOk_1(object sender, CancelEventArgs e)
        {

        }

        private void addDataFolderButton_Click(object sender, EventArgs e)
        {
            if (openDataFolderDialog.ShowDialog() == DialogResult.OK)
            {
                //pictureBox1.Load(openFileDialog1.FileName);
                //dataFolderPath = openDataFolderDialog.SelectedPath;
                ListViewItem item = new ListViewItem(new[] { openDataFolderDialog.SelectedPath });
                dataFolderListView.Items.Add(item);
                openDataFolderDialog.SelectedPath = null;

            }
        }

        private void removeDataFolderButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in dataFolderListView.SelectedItems)
            {
                dataFolderListView.Items.Remove(item);
            }
        }

        private void clearDataFolderButton_Click(object sender, EventArgs e)
        {
            dataFolderListView.Items.Clear();
        }

        private List<String> GetDataFoldersFromListView()
        {
            var dataFolders = new List<String>();
            foreach(ListViewItem item in dataFolderListView.Items)
            {
                dataFolders.Add(item.Text);
            }
            return dataFolders;
        }

        private void selectOutputFolderButton_Click(object sender, EventArgs e)
        {
            if (selectOutputFolderDialog.ShowDialog() == DialogResult.OK)
            {
                outputFolderLabel.Text = selectOutputFolderDialog.SelectedPath;
                selectOutputFolderDialog.SelectedPath = null;
            }
        }

        private void msms_run_button_Click(object sender, EventArgs e)
        {

            progressLabel.Text = "Running MS MS...";
            //// Do not proceed if form is not filled out correctly
            //if (!validateFormOnRun())
            //{
            //    progressLabel.Text = "Invalid form.";
            //    return;
            //}
            try
            {
                //List<IRange> massRanges = GetMassesFromListView();
                List<String> dataFolders = GetDataFoldersFromListView();
                //string outputFolder = outputFolderLabel.Text;
                string outputFolder = "";

                int currentFile = 1;
                int nFiles = dataFolders.Count;
                foreach ( String dataFolder in dataFolders )
                {
                    MSDataReader dr = new MSDataReader(dataFolder, outputFolder, msms: true);
                    progressLabel.Text = String.Format("Processing {0} of {1} files...", currentFile++, nFiles);
                }
                progressLabel.Text = "Done.";

            }
            catch (Exception ex)
            {
                progressLabel.Text = "Fix the error to continue.";
                MessageBox.Show(ex.Message, "Warning");
            }

        }

        private void profileModeCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}