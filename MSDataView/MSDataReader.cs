﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using Agilent.MassSpectrometry.DataAnalysis;

namespace MSDataView
{
    // TODO: uncopy the system libs
    public class MSDataReader 
    {
        public MSDataReader(string dataFile, string outputFolder, List<IRange> massIntervalList = null, bool msms = false, bool profileMode = false)
        {
            println("Starting MSDataReader");
            //foreach (MinMaxRange range in massIntervalList)
            //{
            //    println(String.Format("Mass range: ({0}, {1})", range.Start, range.End));;
            //}
            string msg = null;
            string stackTrace = null;
            try
            {

                //Create an instance of MassSpecDataReader to open up the data file
                println(dataFile);
                IMsdrDataReader dataAccess = new MassSpecDataReader();
                dataAccess.OpenDataFile(dataFile);
                printFileSummaryInfo(dataAccess);
                //println("MHDA API Version: " + dataAccess.Version);
                //println("File Levels: " + dataAccess.MSScanFileInformation.MSLevel.ToString());
                //println("Number of fragmentor voltages in file: " + dataAccess.MSScanFileInformation.FragmentorVoltage.Length.ToString());
                //printArray(dataAccess.MSScanFileInformation.FragmentorVoltage);

                if (msms == true)
                {
                    //DataTable table = dataAccess.FileInformation.MSScanFileInformation.GetMSScanTypeInformation.MSS

                    MinMaxRange range = new MinMaxRange(0, 5000);
                    IRange[] ranges = { range };
                   
                    IBDAChromData[] chroms = GetMSMSInRanges(dataAccess, ranges);
                }
                else
                {
                    //// Why doesn't this work?
                    //IBDAMSScanFileInformation scanFileInfo = dataAccess.FileInformation.MSScanFileInformation;
                    //double minMass = scanFileInfo.MzScanRangeMinimum;
                    //double maxMass = scanFileInfo.MzScanRangeMaximum;
                    //println(String.Format("min Mass detected: {0:0.######}, max Mass detected: {1:0.######}", minMass, maxMass));

                    //    //GetAxisInformation(dataAccess);
                    //PrintFileInfo(dataAccess);
                    //    //PrintDeviceInfo(dataAccess);
                    //    //PrintMSScanInfo(dataAccess);

                    //println("Writing to the following location:");
                    //println(outputFile);
                    //println("");

                    //println("Getting all scans using row index");
                    //GetAllScansUsingRowIndex(dataAccess);//Get all scans using row Index

                    //double[] allMassesInChroms;
                    IRange[] massIntervalArray = massIntervalList.ToArray();

                    //IBDAChromData[] chroms = GetChromsInMassRanges(dataAccess, massIntervalArray, out allMassesInChroms);

                    // Get a chrom for each of the specified ranges
                    IBDAChromData[] chroms = GetChromsFromMassRangeArray(dataAccess, massIntervalArray);
                    double[] massRangeCenterPoints = GetRangeArrayCenterPoints(massIntervalArray);

                    //IBDAChromData[] chroms = GetSummedChromsFromMassRangeList(dataAccess, massIntervalArray);

                    // Print some sample info from the file
                    //PrintChromatogramsSample(chroms);
                    string outputFileName = GetOutputFileInDirectory(dataAccess, outputFolder);

                    WriteChromsToCsv(chroms, massRangeCenterPoints, outputFileName);

                    //if (massIntervalArray.Length == 0)
                    //{
                    //    println("Getting chromatogram using range");
                    //    GetChromInRange(dataAccess, massIntervalArray[0]);
                    //}
                    //else
                    //{
                    //}
                    //println("Getting chromatograms using ranges");


                    //GetAllScansUsingSpecFilter(dataAccess);// See what happens when we use the peak filter
                    //    //GetPrecursorInfo(dataAccess);
                    //    //DeisotopeTest(dataAccess);
                    //    //DoSpectrumExtractionTest(dataAccess);
                    //    //DoSpectrumExtractionTestWithStorage(dataAccess);
                    //    //GetAllScanRecords(dataAccess);
                    //    //GetSampleInfoData(dataAccess);
                    //    //ExtractEIC(dataAccess);


                }
                dataAccess.CloseDataFile();
                println("\nDone.");

            }
            catch (Exception ex)
            {
                msg = ex.Message;
                stackTrace = ex.StackTrace;

                throw ex;
            }
            //println("Press Enter to close");
            //Console.ReadLine();

        }

        private string GetOutputFileInDirectory(IMsdrDataReader dataAccess, string outputFolder)
        {

            IBDAFileInformation fileInfo = dataAccess.FileInformation;
            println("Data File: " + fileInfo.DataFileName);
            println("Output folder: " + outputFolder);
            var dt = DateTime.Now.ToString("MM-dd-HHmmss");

            string fileName = outputFolder + "\\" + Path.GetFileNameWithoutExtension(fileInfo.DataFileName) + "-" + dt + "_CSV.txt";
            println("Output file name: " + fileName);
            return fileName;

        }

        private void WriteChromsToCsv(IBDAChromData[] chroms, double[] allMassesInChroms, string outputFile)
        {
            string del = "\t\t";
            StringBuilder csvString = new StringBuilder();
            // Add first row header
            StringBuilder headerRow = new StringBuilder("RT");
            headerRow.Append(del);

            for (int i = 0; i < allMassesInChroms.Length; i++)
            {
                headerRow.Append(String.Format("Mass{0}", i)).Append(del);
            }

            //headerRow.Append(Environment.NewLine);
            csvString.AppendLine(headerRow.ToString());


            // Add second row header
            StringBuilder massRow = new StringBuilder();
            massRow.Append(del);
            foreach (double mass in allMassesInChroms)
            {
                massRow.AppendFormat("{0}", mass).Append(del);
            }
            //massRow.Append(Environment.NewLine);
            csvString.AppendLine(massRow.ToString());

            var dataMatrix = new StringBuilder(0);
            double[] rt = chroms[0].XArray;
            for (int i = 0; i < rt.Length; i++)
            {
                var dataLine = new StringBuilder(String.Format("{0}", rt[i]));
                dataLine.Append(del);
                foreach (IBDAChromData chrom in chroms)
                {
                    dataLine.AppendFormat("{0}", chrom.YArray[i]).Append(del);
                }
                dataMatrix.AppendLine(dataLine.ToString());

            }
            csvString.AppendLine(dataMatrix.ToString());

            File.WriteAllText(outputFile, csvString.ToString());
            
            
        }

        private double[] GetRangeArrayCenterPoints(IRange[] massIntervalArray)
        {
            List<double> centerPoints = new List<double>();
            foreach (IRange range in massIntervalArray)
            {
                double a = range.Start;
                double b = range.End;
                centerPoints.Add(Math.Round((a + b) / 2, 4));
            }
            return centerPoints.ToArray();
        }

        // Only good with MINMAXRANGEs
        private double[] GetAllMassesInIntervals(IMsdrDataReader dataAccess, IRange[] massIntervalArray)
        {
            double[] allMasses = GetAllScannedMasses(dataAccess);

            List<double> filteredMasses = new List<double>();

            // TODO: How to speed this up???
            foreach (double mass in allMasses)
            {
                foreach (IRange range in massIntervalArray)
                {
                    if (mass > range.Start && mass < range.End)
                    {
                        filteredMasses.Add(mass);
                        break;
                    }
                }
            }
            return filteredMasses.ToArray();

        }

        private double[] GetAllScannedMasses(IMsdrDataReader dataAccess)
        {
            IBDASpecFilter specFilter = new BDASpecFilter();
            // Is this the right spec type??
            specFilter.SpectrumType = SpecType.MassSpectrum;

            IBDASpecData[] spectra = dataAccess.GetSpectrum(specFilter);

            double[] masses = spectra[0].XArray;
            
            //println(String.Format("Number of spectra in spectra: {0}", spectra.Length));
            //println(String.Format("Number of masses in the file: {0}", masses.Length));

            return masses;

        }


        private IRange[] CreateCenterWidthIntervalsFromMasses(double[] masses)
        {
            List<IRange> ranges = new List<IRange>();


            foreach (double mass in masses)
            {
                IRange range = new CenterWidthRange(mass, 1e-6);
                ranges.Add(range);
            }

            return ranges.ToArray();
        }

        private void GetChromInRange(IMsdrDataReader dataAccess, double min, double max)
        {
            MinMaxRange range = new MinMaxRange(min, max);
            GetChromInRange(dataAccess, range);
        }

        private void GetChromInRange(IMsdrDataReader dataAccess, IRange range)
        {
            IRange[] rangeList = new IRange[] { range };
            GetSummedChromsFromMassRangeList(dataAccess, rangeList);
        }

        /// <summary>
        /// Gets a chrom for each range in ranges. If range is out of bounds, it will pick up all zeroes.
        /// </summary>
        /// <param name="dataAccess"></param>
        /// <param name="ranges"></param>
        private IBDAChromData[] GetSummedChromsFromMassRangeList(IMsdrDataReader dataAccess, IRange[] ranges)
        {
            // Initializ the filter and change the settings
            IBDAChromFilter chromFilter = new BDAChromFilter();
            chromFilter.ChromatogramType = ChromType.ExtractedIon;
            chromFilter.MSLevelFilter = MSLevel.MS;

            chromFilter.IncludeMassRanges = ranges;
            IBDAChromData[] chroms = dataAccess.GetChromatogram(chromFilter);

            return chroms;



            // Use ScanRange to limit the time range searched over
            //chromFilter.ScanRange= 

            // This looks like it would be useful if there are multiple fragmentator voltages in one file... 
            // potential cause of future errors?
            //ExtractOneChromatogramPerScanSegment
            //for (int i = 0; i < chroms.Length; i++)
            //{
            //    // do something with each chrom

            //}
            // Use chromFilter.SingleChromatogramForAllMasses = false to get a chrom for each mass range

            // Use ScanRange to limit the time range searched over
            //chromFilter.ScanRange= 
        }

        private IBDAChromData[] GetChromsInMassRanges(IMsdrDataReader dataAccess, 
            IRange[] massIntervalArray, 
            out double[] allMassesInChroms)
        {
            double[] masses = GetAllMassesInIntervals(dataAccess, massIntervalArray);
            allMassesInChroms = masses;

            IRange[] allMassRanges = CreateCenterWidthIntervalsFromMasses(masses);

            // allMassRanges basically has one mass per range, so a summed chrom is really just an individual chrom;
            IBDAChromData[] chroms = GetSummedChromsFromMassRangeList(dataAccess, allMassRanges);
            return chroms;
        }

        private IBDAChromData[] GetChromsFromMassRangeArray(IMsdrDataReader dataAccess, IRange[] massIntervalArray)
        {
            // Get one chromatogram for each range in massIntervalArray
            IBDAChromData[] chroms = GetSummedChromsFromMassRangeList(dataAccess, massIntervalArray);
            return chroms;
        }
        /// <summary>
        /// Prints a sample of each chrom's data (ret time and y value) in the list of chroms.
        /// </summary>
        private void PrintChromatogramsSample(IBDAChromData[] chroms)
        {
            println(String.Format("Printing sample of {0} chromatograms", chroms.Length));

            foreach (IBDAChromData chrom in chroms)
            {

                if (chrom.MeasuredMassRange.Length > 1)
                {
                    println("Why is ther more than one mass range in this chrom?");
                }
                println(String.Format("The measured mass range for this chrom: {0}", GetRangeMidpoint(chrom.MeasuredMassRange[0])));
                double[] retTime = chrom.XArray;
                println("Printing retention time:");
                printArraySample(retTime, retTime.Length);

                // Issues: it may not be EIC
                float[] eic = chrom.YArray;
                println("Printing extracted ion count");
                printArraySample(eic, eic.Length);
                println("");
            }
        }

        private double GetRangeMidpoint(IRange range)
        {
            return (range.End - range.Start) / 2;
        }

        private void ExportChromsToFile(IMsdrDataReader dataAccess, IBDAChromData[] chroms, double[] masses)
        {
            println("do something");

        }

        /// <summary>
        /// Get all scans in the data file using row index.  The storage mode defaults to peakElseProfile.
        /// Passing null for peak filter means do not filter any peaks.
        /// </summary>
        /// <param name="dataAccess"></param>
        private void GetAllScansUsingRowIndex(IMsdrDataReader dataAccess)
        {
            long totalNumScans = dataAccess.MSScanFileInformation.TotalScansPresent;
            for (int i = 0; i < totalNumScans; i++)
            {
                IBDASpecFilter sf = new BDASpecFilter();
                sf.MassRange = new MinMaxRange(50.0, 100.0);

                IBDASpecData spec = dataAccess.GetSpectrum(i, null, null);//no peak filtering
                if (i % 1000 == 0)
                {
                    // This gives us the full spectrum over a mass range
                    // Can use IBDASpecFilter to filter the mass range, but that still gives us a spectrum,
                    // not a chromatogram
                    println("Details from IBDASpecData of scan:");
                    Console.Write(String.Format("Start: {0}, End: {1}", 
                        spec.MeasuredMassRange.Start, spec.MeasuredMassRange.End));
                    println(spec.TotalScanCount.ToString());
                    printArraySample(spec.XArray, spec.TotalDataPoints);
                    printArraySample(spec.YArray, spec.TotalDataPoints);
                    println("");
                }
            }
        }

        /// <summary>
        /// Gets an MSMS chrom for each range in ranges. If range is out of bounds, it will pick up all zeroes.
        /// </summary>
        /// <param name="dataAccess"></param>
        /// <param name="ranges"></param>
        private IBDAChromData[] GetMSMSInRanges(IMsdrDataReader dataAccess, IRange[] ranges=null)
        {

            //// Initializ the filter and change the settings
            IBDAChromFilter chromFilter = new BDAChromFilter();
            //// Can also use TotalIon, BasePeak, ExtractedIon
            chromFilter.ChromatogramType = ChromType.ExtractedIon;
            //chromFilter.ChromatogramType = ChromType.BasePeak;
            chromFilter.MSLevelFilter = MSLevel.MSMS;
            chromFilter.SingleChromatogramForAllMasses = false;
            ////IRange range = new CenterWidthRange(mass, 1e-6);
            //IRange range = new MinMaxRange(20.0, 20000.0);
            //IRange[] rangess = new IRange[] { range };
            //// Supposedly we want mass ranges for EIC, but not BPC, but not for MS/MS?
            chromFilter.IncludeMassRanges = ranges;

            // This is supposed to return a different chrom for each frag voltage, but it doesn't seem to
            chromFilter.ExtractOneChromatogramPerScanSegment = true;
            // This specifies the frag voltage to acquire
            //var fragRange = new CenterWidthRange(355, 1);
            //chromFilter.FragmentorVoltage = fragRange;
            ////chromFilter.MzOfInterestFilter = 

            // This specifies the CE range to acquire
            var ceRange = new CenterWidthRange(10, 1);
            chromFilter.CollisionEnergy = ceRange;

            IBDAChromData[] chroms = dataAccess.GetChromatogram(chromFilter);
            //IBDAChromData[] chroms = null;

            println("number of chroms returned: " + chroms.Length.ToString());
            foreach (var chrom in chroms)
            {
                println("Printing samples of chrom in array ");
                println("MS Level: " + chrom.MSLevelInfo.ToString());
                println("Frag Voltage " + chrom.FragmentorVoltage.ToString());
                println("Collision NRG " + chrom.CollisionEnergy.ToString());
                println("Total Data Points: " + chrom.TotalDataPoints);
                if (chrom.TotalDataPoints > 7)
                {
                    printArraySample(chrom.XArray, chrom.TotalDataPoints);
                    printArraySample(chrom.YArray, chrom.TotalDataPoints);
                }

            }
            //    foreach (IRange range in ranges)
            //{
            //    GetMSMSInRange(dataAccess, range);

            //}


            return chroms;



            // Use ScanRange to limit the time range searched over
            //chromFilter.ScanRange= 

            // This looks like it would be useful if there are multiple fragmentator voltages in one file... 
            // potential cause of future errors?
            //ExtractOneChromatogramPerScanSegment
            //for (int i = 0; i < chroms.Length; i++)
            //{
            //    // do something with each chrom

            //}
            // Use chromFilter.SingleChromatogramForAllMasses = false to get a chrom for each mass range

            // Use ScanRange to limit the time range searched over
            //chromFilter.ScanRange= 
        }


        private void GetMSMSInRange(IMsdrDataReader dataAccess, IRange range)
        {

            //// Initializ the filter and change the settings
            //IBDAChromFilter chromFilter = new BDAChromFilter();
            //// Can also use TotalIon, BasePeak, ExtractedIon
            ////chromFilter.ChromatogramType = ChromType.ExtractedIon;
            //chromFilter.ChromatogramType = ChromType.BasePeak;
            //chromFilter.MSLevelFilter = MSLevel.MSMS;
            ////IRange range = new CenterWidthRange(mass, 1e-6);
            //IRange range = new MinMaxRange(20.0, 20000.0);
            //IRange[] rangess = new IRange[] { range };
            //// Supposedly we want mass ranges for EIC, but not BPC, but not for MS/MS?
            ////chromFilter.IncludeMassRanges = rangess;
            ////chromFilter.MzOfInterestFilter = 

            //IBDAChromData[] chroms = dataAccess.GetChromatogram(chromFilter);
            //long totalNumScans = dataAccess.MSScanFileInformation.TotalScansPresent;
            //println("total number of scans: " + totalNumScans.ToString());
            //for (int i = 0; i < totalNumScans; i++)
            //{
            //    IBDASpecFilter sf = new BDASpecFilter();
            //    sf.MassRange = new MinMaxRange(50.0, 500.0);

            //    IMsdrPeakFilter msfilter = new MsdrPeakFilter();
            //    msfilter.RelativeThreshold = 100.0;
            //    IMsdrPeakFilter msmsfilter = new MsdrPeakFilter();

            //    IBDASpecData spec = dataAccess.GetSpectrum(i, msfilter, msmsfilter);//no peak filtering
            //    println("Spec Level: " + spec.MSLevelInfo.ToString());

            //    ////IBDASpecData spec = dataAccess.GetSpectrum(i, null, null);//no peak filtering
            //    //IBDASpecData spec[] = dataAccess.GetSpectrum(sf);// using the mass range filter
            //    //if (i % 1000 == 0)
            //    //{
            //    //    // This gives us the full spectrum over a mass range
            //    //    // Can use IBDASpecFilter to filter the mass range, but that still gives us a spectrum,
            //    //    // not a chromatogram
            //    //    println("Details from IBDASpecData of scan:");
            //    //    Console.Write(String.Format("Start: {0}, End: {1}",
            //    //        spec.MeasuredMassRange.Start, spec.MeasuredMassRange.End));
            //    //    println(spec.TotalScanCount.ToString());
            //    //    printArraySample(spec.XArray, spec.TotalDataPoints);
            //    //    printArraySample(spec.YArray, spec.TotalDataPoints);
            //    //    println("");
            //    //}
            //}
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////// Helper Functions
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void printArraySample(double[] arr, int len)
        {
            float[] floatArray = Array.ConvertAll(arr, x => (float)x);
            printArraySample(floatArray, len);
        }
        private void printArraySample(float[] arr, int len)
        {
            string prnt = String.Format("{0}, {1}, {2}, ... , {3}, {4}, {5}: {6} points\n",
                arr[0], arr[1], arr[2], arr[len - 3], arr[len - 2], arr[len - 1], len);
            Console.Write(prnt);
        }

        private void printArray(double[] arr)
        {
            foreach (double num in arr)
            {
                print(num.ToString() + ", ");
            }
            println("");
        }

        private void PrintDeviceInfo(IMsdrDataReader dataAccess)
        {
            println("Device Table: ");

            DataTable table = dataAccess.FileInformation.GetDeviceTable(StoredDataType.All);
            StringBuilder buf = new StringBuilder();

            foreach (DataRow r in table.Rows)
            {
                foreach (DataColumn c in table.Columns)
                {
                    buf.AppendLine("\t" + c.ColumnName + ": " + r[c].ToString());
                }
                buf.AppendLine();
            }
            print(buf.ToString());
        }


        private void PrintFileInfo(IMsdrDataReader dataAccess)
        {
            IBDAFileInformation fileInfo = dataAccess.FileInformation;
            println("Data File: " + fileInfo.DataFileName);
            println("Acquisition Time: " + fileInfo.AcquisitionTime.ToShortDateString());
            println("IRM Status: " + fileInfo.IRMStatus);
            println("Measurement Type: " + fileInfo.MeasurementType);
            println("Separation Technique: " + fileInfo.SeparationTechnique);

            print("MS Data Present: ");
            if (fileInfo.IsMSDataPresent())
                println("yes");
            else
                println("no");

            print("Non-MS data present: ");
            if (fileInfo.IsNonMSDataPresent())
                println("yes");
            else
                println("no");

            print("UV spectra present: ");
            if (fileInfo.IsUVSpectralDataPresent())
                println("yes");
            else
                println("no");

        }

        private void printFileSummaryInfo(IMsdrDataReader dataAccess)
        {
            println("");
            println("//////////////////////////////////////////////////////");
            println("FILE SUMMARY");
            println("MHDA API Version: " + dataAccess.Version);
            IBDAMSScanFileInformation info = dataAccess.MSScanFileInformation;
            println("File Levels: " + info.MSLevel.ToString());
            println("Number of fragmentor voltages in file: " + info.FragmentorVoltage.Length.ToString());
            print("Fragmentor voltages in file: ");
            printArray(info.FragmentorVoltage);
            println("Number of Collision energies in file: " + info.CollisionEnergy.Length.ToString());
            print("collision nrg in file: ");
            printArray(info.CollisionEnergy);
            println("//////////////////////////////////////////////////////");
            println("");
        }

        /// <summary>
        /// Get all scans in the data file using row index.  The storage mode defaults to peakElseProfile.
        /// Passing null for peak filter means do not filter any peaks.
        /// </summary>
        /// <param name="dataAccess"></param>
        //private void GetAllScansUsingSpecFilter(IMsdrDataReader dataAccess)
        //{
        //    long totalNumScans = dataAccess.MSScanFileInformation.TotalScansPresent;
        //    for (int i = 0; i < totalNumScans; i++)
        //    {
        //        IBDASpecFilter sf = new BDASpecFilter();
        //        sf.MassRange = new MinMaxRange(50.0, 100.0);
                
        //        //IBDASpecData spec = dataAccess.GetSpectrum(i, null, null);//no peak filtering
        //        IBDASpecData spec[] = dataAccess.GetSpectrum(sf);// using the mass range filter
        //        if (i % 1000 == 0)
        //        {
        //            // This gives us the full spectrum over a mass range
        //            // Can use IBDASpecFilter to filter the mass range, but that still gives us a spectrum,
        //            // not a chromatogram
        //            println("Details from IBDASpecData of scan:");
        //            Console.Write(String.Format("Start: {0}, End: {1}", 
        //                spec.MeasuredMassRange.Start, spec.MeasuredMassRange.End));
        //            println(spec.TotalScanCount.ToString());
        //            printArraySample(spec.XArray, spec.TotalDataPoints);
        //            printArraySample(spec.YArray, spec.TotalDataPoints);
        //            println("");
        //        }
        //    }
        //}

        private void print(string s)
        {
            Console.Write(s);
        }

        private void println(string s)
        {
            Console.WriteLine(s);
        }
    }
}
