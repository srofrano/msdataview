﻿namespace MSDataView
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectDataFolderButton = new System.Windows.Forms.Button();
            this.clearDataFolderButton = new System.Windows.Forms.Button();
            this.runButton = new System.Windows.Forms.Button();
            this.startMassTextBox = new System.Windows.Forms.TextBox();
            this.endMassTextBox = new System.Windows.Forms.TextBox();
            this.addIntervalButton = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openDataFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.removeIntervalButton = new System.Windows.Forms.Button();
            this.startMassLabel = new System.Windows.Forms.Label();
            this.endMassLabel = new System.Windows.Forms.Label();
            this.loadIntervalsButton = new System.Windows.Forms.Button();
            this.saveIntervalsFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.massIntervalListView = new System.Windows.Forms.ListView();
            this.startMassHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.endMassHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.saveIntervalsButton = new System.Windows.Forms.Button();
            this.saveIntervalsFileLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.toleranceLabel = new System.Windows.Forms.Label();
            this.toleranceTextBox = new System.Windows.Forms.TextBox();
            this.dataFolderListView = new System.Windows.Forms.ListView();
            this.dataFolderHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.removeDataFolderButton = new System.Windows.Forms.Button();
            this.selectOutputFolderButton = new System.Windows.Forms.Button();
            this.outputFolderLabel = new System.Windows.Forms.Label();
            this.progressLabel = new System.Windows.Forms.Label();
            this.msms_run_button = new System.Windows.Forms.Button();
            this.selectOutputFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.loadRangeFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.profileModeCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // selectDataFolderButton
            // 
            this.selectDataFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.selectDataFolderButton.AutoSize = true;
            this.selectDataFolderButton.Location = new System.Drawing.Point(180, 95);
            this.selectDataFolderButton.Name = "selectDataFolderButton";
            this.selectDataFolderButton.Size = new System.Drawing.Size(112, 23);
            this.selectDataFolderButton.TabIndex = 0;
            this.selectDataFolderButton.Text = "Add Data";
            this.selectDataFolderButton.UseVisualStyleBackColor = true;
            this.selectDataFolderButton.Click += new System.EventHandler(this.addDataFolderButton_Click);
            // 
            // clearDataFolderButton
            // 
            this.clearDataFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.clearDataFolderButton.AutoSize = true;
            this.clearDataFolderButton.Location = new System.Drawing.Point(1008, 95);
            this.clearDataFolderButton.Name = "clearDataFolderButton";
            this.clearDataFolderButton.Size = new System.Drawing.Size(112, 23);
            this.clearDataFolderButton.TabIndex = 1;
            this.clearDataFolderButton.Text = "Clear";
            this.clearDataFolderButton.UseVisualStyleBackColor = true;
            this.clearDataFolderButton.Click += new System.EventHandler(this.clearDataFolderButton_Click);
            // 
            // runButton
            // 
            this.runButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.runButton.Location = new System.Drawing.Point(1008, 665);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(62, 23);
            this.runButton.TabIndex = 4;
            this.runButton.Text = "Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // startMassTextBox
            // 
            this.startMassTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.startMassTextBox.Location = new System.Drawing.Point(180, 309);
            this.startMassTextBox.Name = "startMassTextBox";
            this.startMassTextBox.Size = new System.Drawing.Size(112, 20);
            this.startMassTextBox.TabIndex = 8;
            // 
            // endMassTextBox
            // 
            this.endMassTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.endMassTextBox.Location = new System.Drawing.Point(180, 380);
            this.endMassTextBox.Name = "endMassTextBox";
            this.endMassTextBox.Size = new System.Drawing.Size(112, 20);
            this.endMassTextBox.TabIndex = 9;
            // 
            // addIntervalButton
            // 
            this.addIntervalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.addIntervalButton.Location = new System.Drawing.Point(180, 450);
            this.addIntervalButton.Name = "addIntervalButton";
            this.addIntervalButton.Size = new System.Drawing.Size(112, 23);
            this.addIntervalButton.TabIndex = 10;
            this.addIntervalButton.Text = "Add Interval";
            this.addIntervalButton.UseVisualStyleBackColor = true;
            this.addIntervalButton.Click += new System.EventHandler(this.addIntervalButton_Click);
            // 
            // openDataFolderDialog
            // 
            this.openDataFolderDialog.RootFolder = System.Environment.SpecialFolder.MyDocuments;
            this.openDataFolderDialog.ShowNewFolderButton = false;
            this.openDataFolderDialog.HelpRequest += new System.EventHandler(this.openDataFolderDialog_HelpRequest);
            // 
            // removeIntervalButton
            // 
            this.removeIntervalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.removeIntervalButton.Location = new System.Drawing.Point(180, 521);
            this.removeIntervalButton.Name = "removeIntervalButton";
            this.removeIntervalButton.Size = new System.Drawing.Size(112, 23);
            this.removeIntervalButton.TabIndex = 11;
            this.removeIntervalButton.Text = "Remove Interval";
            this.removeIntervalButton.UseVisualStyleBackColor = true;
            this.removeIntervalButton.Click += new System.EventHandler(this.removeIntervalButton_Click);
            // 
            // startMassLabel
            // 
            this.startMassLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.startMassLabel.AutoSize = true;
            this.startMassLabel.Location = new System.Drawing.Point(62, 313);
            this.startMassLabel.Name = "startMassLabel";
            this.startMassLabel.Size = new System.Drawing.Size(112, 13);
            this.startMassLabel.TabIndex = 12;
            this.startMassLabel.Text = "Start Mass:";
            this.startMassLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // endMassLabel
            // 
            this.endMassLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.endMassLabel.AutoSize = true;
            this.endMassLabel.Location = new System.Drawing.Point(62, 384);
            this.endMassLabel.Name = "endMassLabel";
            this.endMassLabel.Size = new System.Drawing.Size(112, 13);
            this.endMassLabel.TabIndex = 13;
            this.endMassLabel.Text = "End Mass:";
            this.endMassLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // loadIntervalsButton
            // 
            this.loadIntervalsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.loadIntervalsButton.Location = new System.Drawing.Point(180, 592);
            this.loadIntervalsButton.Name = "loadIntervalsButton";
            this.loadIntervalsButton.Size = new System.Drawing.Size(112, 23);
            this.loadIntervalsButton.TabIndex = 14;
            this.loadIntervalsButton.Text = "Load Intervals...";
            this.loadIntervalsButton.UseVisualStyleBackColor = true;
            this.loadIntervalsButton.Click += new System.EventHandler(this.loadIntervalsButton_Click);
            // 
            // massIntervalListView
            // 
            this.massIntervalListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.startMassHeader,
            this.endMassHeader});
            this.tableLayoutPanel3.SetColumnSpan(this.massIntervalListView, 2);
            this.massIntervalListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.massIntervalListView.FullRowSelect = true;
            this.massIntervalListView.Location = new System.Drawing.Point(298, 216);
            this.massIntervalListView.Name = "massIntervalListView";
            this.tableLayoutPanel3.SetRowSpan(this.massIntervalListView, 5);
            this.massIntervalListView.Size = new System.Drawing.Size(704, 349);
            this.massIntervalListView.TabIndex = 7;
            this.massIntervalListView.UseCompatibleStateImageBehavior = false;
            this.massIntervalListView.View = System.Windows.Forms.View.Details;
            // 
            // startMassHeader
            // 
            this.startMassHeader.Text = "Start Mass";
            this.startMassHeader.Width = 62;
            // 
            // endMassHeader
            // 
            this.endMassHeader.Text = "End Mass";
            this.endMassHeader.Width = 172;
            // 
            // saveIntervalsButton
            // 
            this.saveIntervalsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.saveIntervalsButton.Location = new System.Drawing.Point(298, 592);
            this.saveIntervalsButton.Name = "saveIntervalsButton";
            this.saveIntervalsButton.Size = new System.Drawing.Size(112, 23);
            this.saveIntervalsButton.TabIndex = 15;
            this.saveIntervalsButton.Text = "Save Intervals...";
            this.saveIntervalsButton.UseVisualStyleBackColor = true;
            this.saveIntervalsButton.Click += new System.EventHandler(this.saveIntervalsButton_Click);
            // 
            // saveIntervalsFileLabel
            // 
            this.saveIntervalsFileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.saveIntervalsFileLabel.AutoSize = true;
            this.saveIntervalsFileLabel.Location = new System.Drawing.Point(416, 597);
            this.saveIntervalsFileLabel.Name = "saveIntervalsFileLabel";
            this.saveIntervalsFileLabel.Size = new System.Drawing.Size(586, 13);
            this.saveIntervalsFileLabel.TabIndex = 11;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel3.Controls.Add(this.saveIntervalsButton, 3, 8);
            this.tableLayoutPanel3.Controls.Add(this.saveIntervalsFileLabel, 4, 8);
            this.tableLayoutPanel3.Controls.Add(this.runButton, 5, 9);
            this.tableLayoutPanel3.Controls.Add(this.massIntervalListView, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.loadIntervalsButton, 2, 8);
            this.tableLayoutPanel3.Controls.Add(this.removeIntervalButton, 2, 7);
            this.tableLayoutPanel3.Controls.Add(this.addIntervalButton, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.endMassTextBox, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.startMassTextBox, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.endMassLabel, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.startMassLabel, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.toleranceLabel, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.toleranceTextBox, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.dataFolderListView, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.clearDataFolderButton, 5, 1);
            this.tableLayoutPanel3.Controls.Add(this.selectDataFolderButton, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.removeDataFolderButton, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.selectOutputFolderButton, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.outputFolderLabel, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.progressLabel, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.msms_run_button, 3, 9);
            this.tableLayoutPanel3.Controls.Add(this.profileModeCheckBox, 5, 8);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 10;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1184, 714);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // toleranceLabel
            // 
            this.toleranceLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.toleranceLabel.AutoSize = true;
            this.toleranceLabel.Location = new System.Drawing.Point(62, 242);
            this.toleranceLabel.Name = "toleranceLabel";
            this.toleranceLabel.Size = new System.Drawing.Size(112, 13);
            this.toleranceLabel.TabIndex = 18;
            this.toleranceLabel.Text = "Tolerance (ppm):";
            this.toleranceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toleranceTextBox
            // 
            this.toleranceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.toleranceTextBox.Location = new System.Drawing.Point(180, 238);
            this.toleranceTextBox.Name = "toleranceTextBox";
            this.toleranceTextBox.Size = new System.Drawing.Size(112, 20);
            this.toleranceTextBox.TabIndex = 19;
            this.toleranceTextBox.TextChanged += new System.EventHandler(this.toleranceTextBox_TextChanged);
            // 
            // dataFolderListView
            // 
            this.dataFolderListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataFolderListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dataFolderHeader});
            this.tableLayoutPanel3.SetColumnSpan(this.dataFolderListView, 2);
            this.dataFolderListView.Location = new System.Drawing.Point(298, 74);
            this.dataFolderListView.Name = "dataFolderListView";
            this.tableLayoutPanel3.SetRowSpan(this.dataFolderListView, 2);
            this.dataFolderListView.Size = new System.Drawing.Size(704, 136);
            this.dataFolderListView.TabIndex = 20;
            this.dataFolderListView.UseCompatibleStateImageBehavior = false;
            this.dataFolderListView.View = System.Windows.Forms.View.Details;
            // 
            // dataFolderHeader
            // 
            this.dataFolderHeader.Text = "Data Folder";
            this.dataFolderHeader.Width = 600;
            // 
            // removeDataFolderButton
            // 
            this.removeDataFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.removeDataFolderButton.AutoSize = true;
            this.removeDataFolderButton.Location = new System.Drawing.Point(180, 166);
            this.removeDataFolderButton.Name = "removeDataFolderButton";
            this.removeDataFolderButton.Size = new System.Drawing.Size(112, 23);
            this.removeDataFolderButton.TabIndex = 21;
            this.removeDataFolderButton.Text = "Remove Data Folder";
            this.removeDataFolderButton.UseVisualStyleBackColor = true;
            this.removeDataFolderButton.Click += new System.EventHandler(this.removeDataFolderButton_Click);
            // 
            // selectOutputFolderButton
            // 
            this.selectOutputFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.selectOutputFolderButton.AutoSize = true;
            this.selectOutputFolderButton.Location = new System.Drawing.Point(180, 24);
            this.selectOutputFolderButton.Name = "selectOutputFolderButton";
            this.selectOutputFolderButton.Size = new System.Drawing.Size(112, 23);
            this.selectOutputFolderButton.TabIndex = 22;
            this.selectOutputFolderButton.Text = "Select Output Folder";
            this.selectOutputFolderButton.UseVisualStyleBackColor = true;
            this.selectOutputFolderButton.Click += new System.EventHandler(this.selectOutputFolderButton_Click);
            // 
            // outputFolderLabel
            // 
            this.outputFolderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.outputFolderLabel.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.outputFolderLabel, 2);
            this.outputFolderLabel.Location = new System.Drawing.Point(298, 29);
            this.outputFolderLabel.Name = "outputFolderLabel";
            this.outputFolderLabel.Size = new System.Drawing.Size(704, 13);
            this.outputFolderLabel.TabIndex = 23;
            // 
            // progressLabel
            // 
            this.progressLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressLabel.AutoSize = true;
            this.progressLabel.Location = new System.Drawing.Point(3, 384);
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(53, 13);
            this.progressLabel.TabIndex = 17;
            this.progressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // msms_run_button
            // 
            this.msms_run_button.Location = new System.Drawing.Point(298, 642);
            this.msms_run_button.Name = "msms_run_button";
            this.msms_run_button.Size = new System.Drawing.Size(75, 23);
            this.msms_run_button.TabIndex = 24;
            this.msms_run_button.Text = "Run MS/MS";
            this.msms_run_button.UseVisualStyleBackColor = true;
            this.msms_run_button.Click += new System.EventHandler(this.msms_run_button_Click);
            // 
            // selectOutputFolderDialog
            // 
            this.selectOutputFolderDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // loadRangeFileDialog
            // 
            this.loadRangeFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.loadRangeFileDialog_FileOk_1);
            // 
            // profileModeCheckBox
            // 
            this.profileModeCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.profileModeCheckBox.AutoSize = true;
            this.profileModeCheckBox.Location = new System.Drawing.Point(1008, 595);
            this.profileModeCheckBox.Name = "profileModeCheckBox";
            this.profileModeCheckBox.Size = new System.Drawing.Size(85, 17);
            this.profileModeCheckBox.TabIndex = 25;
            this.profileModeCheckBox.Text = "Profile Mode";
            this.profileModeCheckBox.UseVisualStyleBackColor = true;
            this.profileModeCheckBox.CheckedChanged += new System.EventHandler(this.profileModeCheckBox_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 714);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Name = "Form1";
            this.Text = "MS Data View";
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button selectDataFolderButton;
        private System.Windows.Forms.Button clearDataFolderButton;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.FolderBrowserDialog openDataFolderDialog;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.TextBox startMassTextBox;
        private System.Windows.Forms.TextBox endMassTextBox;
        private System.Windows.Forms.Button addIntervalButton;
        private System.Windows.Forms.Button removeIntervalButton;
        private System.Windows.Forms.Label endMassLabel;
        private System.Windows.Forms.Label startMassLabel;
        private System.Windows.Forms.Button loadIntervalsButton;
        private System.Windows.Forms.SaveFileDialog saveIntervalsFileDialog;
        private System.Windows.Forms.Label saveIntervalsFileLabel;
        private System.Windows.Forms.Button saveIntervalsButton;
        private System.Windows.Forms.ListView massIntervalListView;
        private System.Windows.Forms.ColumnHeader startMassHeader;
        private System.Windows.Forms.ColumnHeader endMassHeader;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label progressLabel;
        private System.Windows.Forms.Label toleranceLabel;
        private System.Windows.Forms.TextBox toleranceTextBox;
        private System.Windows.Forms.FolderBrowserDialog selectOutputFolderDialog;
        private System.Windows.Forms.OpenFileDialog loadRangeFileDialog;
        private System.Windows.Forms.ListView dataFolderListView;
        private System.Windows.Forms.ColumnHeader dataFolderHeader;
        private System.Windows.Forms.Button removeDataFolderButton;
        private System.Windows.Forms.Button selectOutputFolderButton;
        private System.Windows.Forms.Label outputFolderLabel;
        private System.Windows.Forms.Button msms_run_button;
        private System.Windows.Forms.CheckBox profileModeCheckBox;
    }
}

